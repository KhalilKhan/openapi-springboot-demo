package com.warwolf.openapidemo;

import java.util.Optional;

import org.openapitools.api.UsersApi;
import org.openapitools.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController implements UsersApi{

    @Autowired
    UserService userService;

    @Override
    public ResponseEntity<User> createUser(User user, Optional<String> xApiVersion) {
        return new ResponseEntity<User>(userService.creatUser(user), HttpStatus.CREATED);
    }
}
