package com.warwolf.openapidemo;

import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class UserRepository {
    private static final Map<Long,UserEntity> usersTable = new ConcurrentHashMap<>();
    private AtomicLong atomicLong = new AtomicLong();
    
    public UserEntity saveUser(UserEntity userEntity){
        long currId=atomicLong.getAndIncrement();
        if(!usersTable.containsKey(currId)){
            userEntity.setId(currId);
            usersTable.put(currId, userEntity);
            return userEntity;
        }
        return userEntity;
    }

    public UserEntity findById(Long id){
        UserEntity naUser = new UserEntity();
        naUser.setAge(-1);
        naUser.setEmail("NA");
        naUser.setFirstName("NA");
        naUser.setLastName("NA");
        naUser.setId(-1l);
        return usersTable.getOrDefault(id, naUser);
    }
}
